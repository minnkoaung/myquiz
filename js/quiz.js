(function(){

	var app = angular.module('myQuiz',[]);

	app.controller('QuizController', ['$scope','$http','$sce',function($scope,$http,$sce){
		
		/*
		*** Scope ***
		Scope is an object that refers to the application model. 
		It is an execution context for expressions. Scopes are arranged in hierarchical
		structure which mimic the DOM structure of the application.
		Scopes can watch expressions and propagate events.
		API Ref : https://docs.angularjs.org/api/ng/type/$rootScope.Scope
		*/

		/*
		*** http ***
		The $http service is a core Angular service that facilitates 
		communication with the remote HTTP servers via the browser's 
		XMLHttpRequest object or via JSONP.
		API Ref :https://docs.angularjs.org/api/ng/service/$http
		*/

		/* 
		*** sec ***
		Strict Contextual Escaping (SCE) is
		a mode in which AngularJS requires bindings in certain contexts to 
		result in a value that is marked as safe to use for that context. 
		One example of such a context is binding arbitrary html controlled 
		by the user via ng-bind-html. We refer to these contexts as privileged 
		or SCE contexts.
		API Ref :https://docs.angularjs.org/api/ng/service/$sce
		*/
		

		$scope.score = 0;
		$scope.activeQuestion = -1;
		$scope.activeQuestionAnswered = 0;
		$scope.percentage= 0;

		//loading json file
		$http.get('quiz_data.json').then(function(quizData){
			$scope.myQuestions = quizData.data;
			$scope.totalQuestions = $scope.myQuestions.length;
		});


		$scope.selectAnswer =  function(qIndex,aIndex){

			//console.log(qIndex + 'and' +aIndex);

			var questionState = $scope.myQuestions[qIndex].questionState;

			if(questionState != 'answered'){
				$scope.myQuestions[qIndex].selectedAnswer = aIndex;
				var correctAnswer = $scope.myQuestions[qIndex].correct;
				$scope.myQuestions[qIndex].correctAnswer = correctAnswer;

				if(aIndex === correctAnswer){
					$scope.myQuestions[qIndex].correctness = 'correct';
					$scope.score += 1;
				}else{
					$scope.myQuestions[qIndex].correctness = 'incorrect';
				}
				$scope.myQuestions[qIndex].questionState = 'answered';
			}
			$scope.percentage = (($scope.score / $scope.totalQuestions)*100).toFixed(2);

		}

		$scope.isSelected = function(qIndex,aIndex){
			return $scope.myQuestions[qIndex].selectedAnswer === aIndex;
		}

		$scope.isCorrect = function(qIndex,aIndex){
			return $scope.myQuestions[qIndex].correctAnswer === aIndex;
		}

		$scope.selectContinue = function(){
			return $scope.activeQuestion += 1;
		}

		$scope.createShareLinks = function(percentage){
			var url = 'http://yoursite.com';
			var emailLink = '<a class="btn email" herf="mailto:?subject=Try to beat my quiz score!&amp;body=I score a '+percentage+'% on this quiz about Saturn. Try to beat my score at '+url+'">Email a Friend</a>';
			var twitterLink = '<a class="btn twitter" target="_blank" herf="http://twitter.com/share?text=I scored a '+percentage+'% on this about Saturn. Try to beat my score at&amp; $hashtag=SaturnQuiz$url='+url+'">Tweet your score</a>';
			var newMarkup = emailLink + twitterLink;
			return $sce.trustAsHtml(newMarkup);
		}

	}]);

		



})();